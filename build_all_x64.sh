# set working Directory
cd "${0%/*}"

# Update repository
cd citeproc-js-server
git pull --recurse-submodules
cd ..

# Remove old builds
rm releases/*

# Replace package.json
rm citeproc-js-server/package.json
cp package.json citeproc-js-server/package.json

# Create copy to work on
cp -R citeproc-js-server _building

# Install dependencies
cd citeproc-js-server
npm install .
cd ..

# Build Linux version
pkg --target node8-linux-x64 --output ./_building/citeproc citeproc-js-server
cd _building
tar -czvf ../releases/citeproc-js-server_linux-x64.tar.gz *
rm citeproc
cd ..

# Build Windows version
pkg --target node8-win-x64 --output ./_building/citeproc.exe citeproc-js-server
cd _building
zip -r ../releases/citeproc-js-server_windows-x64.zip *
rm citeproc.exe
cd ..

# Build macOS version
pkg --target node8-macos-x64 --output ./_building/citeproc citeproc-js-server
cd _building
tar -czvf ../releases/citeproc-js-server_macos-x64.tar.gz *
rm citeproc
cd ..

# Cleanup
cd citeproc-js-server
git stash
rm -rf node_modules
cd ..
rm -rf _building
